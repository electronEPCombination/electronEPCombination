// EpCombination includes
#include "EpCombination/EpCombination.h"
#include "EpCombination/CombinedPT_calculator.h"

namespace CP {

EpCombination::EpCombination( const std::string& name ) : asg::AsgTool( name ){

  declareProperty( "mcPeriod",  m_mcPeriod = 0  , "MC realse to use for the likelihood functions / PDF" );
  declareProperty( "useGeV",    m_useGeV = false  , "units of the momentum and Energy are in GeV if true (MeV if false)" );

  declareProperty( "useDebugMessages",    m_useDebugMessages = false  , "display debug messages" );


}

EpCombination::~EpCombination()
{
  if (m_useDebugMessages) std::cout << "Destructing CombinedPT_manager!"<< std::endl;
/*
  delete LC_LB;
  delete LM_LB;
  delete LCr_LB;
  delete LF_LB;
  delete MC_LB;
  delete MM_LB;
  delete MCr_LB;
  delete MF_LB;
  delete HC_LB;
  delete HM_LB;
  delete HCr_LB;
  delete HF_LB;
  delete LC_HB;
  delete LM_HB;
  delete LCr_HB;
  delete LF_HB;
  delete MC_HB;
  delete MM_HB;
  delete MCr_HB;
  delete MF_HB;
  delete HC_HB;
  delete HM_HB;
  delete HCr_HB;
  delete HF_HB;
*/
  
} // EpCombination::~EpCombination()


StatusCode EpCombination::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << ", mcPeriod = " << m_mcPeriod <<  "...");
  //
  //Make use of the property values to configure the tool
  //Tools should be designed so that no method other than setProperty is called before initialize
  //

      if (m_mcPeriod == 0) {

    std::cout << "CombinedPT_manager: Using test PDFs " << std::endl; // 
   
    //PDFs for testing purposes, used in the example
//                                   meanCalo, meanTrack, sigmaCalo, sigmaTrack, aCalo, aTrack, nCalo, nTrack, meanGCalo, meanGTrack, sigmaGCalo, sigmaGTrack, caloCBWeight, trackCBWeight,
LC_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LC_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LM_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LM_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LCr_LB  = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LCr_HB  = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LF_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
LF_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MC_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MC_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MM_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MM_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MCr_LB  = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MCr_HB  = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MF_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
MF_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HC_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HC_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HM_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HM_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HCr_LB  = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HCr_HB  = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HF_LB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages);
HF_HB   = new CombinedPT_calculator(     0.99,      0.98,      0.05,        0.1,   1.4,    0.3,   4.0,  14.0 ,       1.0,        0.9,       0.06,         0.2,         0.94,          0.98,   m_useDebugMessages); 

  } //  if (m_mcPeriod == 0) 

    else if (m_mcPeriod == 2016) {

    std::cout << "CombinedPT_manager: Using MC16a PDFs " << std::endl; // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EgammaCalibration#MC15c_20_7_5_single_particle_sam
   
    //PDF for MC16a (21.?.?) 
//                    meanCalo, meanTrack,  sigmaCalo,  sigmaTrack, aCalo,    aTrack,   nCalo,    nTrack,   meanGCalo,  meanGTrack, sigmaGCalo, sigmaGTrack,  caloCBWeight, trackCBWeight,
vLC_LB  = new CombinedPT_calculator(  0.995455, 0.952527, 0.055876, 0.060958, 1.515472, 0.863814, 1.907991, 0.607251, 1.041055, 1.001090, 0.108486, 0.019208, 0.780655, 0.542723, m_useDebugMessages);
vLC_HB  = new CombinedPT_calculator(  0.978001, 0.936474, 0.066259, 0.084948, 0.968797, 0.807880, 15.083635,  0.449395, 1.146902, 1.000767, 0.044159, 0.025658, 0.985445, 0.609527, m_useDebugMessages);
vLM_LB  = new CombinedPT_calculator(  1.001965, 0.946940, 0.063707, 0.074972, 0.366994, 0.704700, 19.999956,  0.632219, 1.028275, 1.000708, 0.106887, 0.026661, 0.237288, 0.654154, m_useDebugMessages);
vLM_HB  = new CombinedPT_calculator(  0.936383, 0.996831, 0.102160, 0.035889, 1.238764, 0.777353, 2.531818, 0.486183, 0.991056, 1.013853, 0.116642, 0.085453, 0.473107, 0.879543, m_useDebugMessages);
vLCr_LB = new CombinedPT_calculator(  1.196741, 0.998133, 0.002326, 0.035910, 0.003398, 0.619056, 19.995921,  0.477022, 1.000925, 1.006229, 0.135683, 0.081583, 0.180055, 0.872940, m_useDebugMessages);
vLCr_HB = new CombinedPT_calculator(  0.901228, 0.986108, 0.074358, 0.056682, 1.281002, 0.905537, 0.000003, 0.115097, 0.950314, 0.975442, 0.145684, 0.182205, 0.146070, 0.867907, m_useDebugMessages);
vLF_LB  = new CombinedPT_calculator(  1.002265, 0.900000, 0.044388, 0.129936, 0.742066, 0.363539, 3.563237, 0.379937, 1.083777, 1.004680, 0.115394, 0.050298, 0.847077, 0.841762, m_useDebugMessages);
vLF_HB  = new CombinedPT_calculator(  0.987636, 0.927477, 0.050042, 0.149490, 0.567183, 0.326471, 6.058409, 0.201656, 1.027383, 1.013626, 0.122592, 0.045847, 0.763954, 0.966842, m_useDebugMessages);
LC_LB = new CombinedPT_calculator(  0.973466, 0.960461, 0.077383, 0.048071, 1.832549, 0.871540, 1.450166, 0.577852, 1.001819, 1.001923, 0.034778, 0.018702, 0.249330, 0.598952, m_useDebugMessages);
LC_HB = new CombinedPT_calculator(  0.994247, 0.946900, 0.037341, 0.078669, 1.271691, 0.760610, 3.010080, 0.514091, 0.971237, 1.003066, 0.071640, 0.027269, 0.771544, 0.641088, m_useDebugMessages);
LM_LB = new CombinedPT_calculator(  1.007450, 0.947515, 0.047762, 0.064959, 1.231214, 0.723798, 2.381220, 0.504453, 0.998452, 1.000985, 0.079933, 0.026491, 0.500405, 0.677101, m_useDebugMessages);
LM_HB = new CombinedPT_calculator(  0.961383, 0.930267, 0.085229, 0.090809, 1.750306, 0.653096, 2.553173, 0.362892, 1.000734, 1.001857, 0.047926, 0.032843, 0.730651, 0.721707, m_useDebugMessages);
LCr_LB  = new CombinedPT_calculator(  1.043306, 0.990482, 0.110188, 0.044512, 1.024082, 0.586232, 19.999983,  0.629820, 0.988267, 0.900099, 0.092923, 0.200000, 0.641911, 0.965289, m_useDebugMessages);
LCr_HB  = new CombinedPT_calculator(  0.970657, 0.900000, 0.101493, 0.122128, 1.156101, 0.438444, 19.787640,  0.237509, 1.033766, 1.002094, 0.147152, 0.044280, 0.725241, 0.821499, m_useDebugMessages);
LF_LB = new CombinedPT_calculator(  1.002292, 0.900000, 0.030419, 0.123890, 0.736521, 0.466854, 4.082955, 0.209875, 1.048625, 1.007902, 0.081735, 0.051577, 0.839671, 0.858212, m_useDebugMessages);
LF_HB = new CombinedPT_calculator(  0.995238, 0.966281, 0.033931, 0.122592, 0.603633, 0.325761, 5.705330, 0.148764, 1.025918, 1.152345, 0.089577, 0.195961, 0.791804, 1.000000, m_useDebugMessages);
MC_LB = new CombinedPT_calculator(  0.973812, 0.961763, 0.049563, 0.045733, 1.680767, 0.852445, 2.147141, 0.638757, 0.999824, 1.003347, 0.022177, 0.019580, 0.173504, 0.647431, m_useDebugMessages);
MC_HB = new CombinedPT_calculator(  0.967362, 0.955339, 0.047807, 0.081445, 1.767002, 0.749202, 2.240168, 0.486649, 0.997581, 1.007829, 0.022708, 0.033021, 0.246331, 0.692478, m_useDebugMessages);
MM_LB = new CombinedPT_calculator(  0.976891, 0.944430, 0.057532, 0.065805, 1.826973, 0.722690, 2.156610, 0.474415, 1.003097, 1.001639, 0.030493, 0.028555, 0.348103, 0.689549, m_useDebugMessages);
MM_HB = new CombinedPT_calculator(  0.996384, 0.943428, 0.031980, 0.088043, 1.136103, 0.628350, 3.443792, 0.382633, 0.979094, 1.006563, 0.053338, 0.035870, 0.619352, 0.755853, m_useDebugMessages);
MCr_LB  = new CombinedPT_calculator(  1.001028, 0.993369, 0.052716, 0.041109, 0.968441, 0.465255, 9.192577, 0.544478, 1.033245, 0.985252, 0.095170, 0.075368, 0.543053, 0.889018, m_useDebugMessages);
MCr_HB  = new CombinedPT_calculator(  0.988231, 0.908293, 0.072373, 0.120222, 1.240556, 0.397696, 19.999998,  0.264065, 1.171513, 1.010127, 0.049008, 0.050267, 0.968163, 0.813616, m_useDebugMessages);
MF_LB = new CombinedPT_calculator(  0.976516, 0.900000, 0.065011, 0.124053, 1.538115, 0.379079, 3.846562, 0.170541, 0.998827, 1.011980, 0.020975, 0.052217, 0.356515, 0.866721, m_useDebugMessages);
MF_HB = new CombinedPT_calculator(  0.971990, 0.974293, 0.066726, 0.125183, 1.495121, 0.333325, 4.167515, 0.057890, 0.995587, 1.125844, 0.022848, 0.199991, 0.431686, 1.000000, m_useDebugMessages);
HC_LB = new CombinedPT_calculator(  0.999956, 0.957555, 0.009771, 0.082347, 1.598061, 0.764285, 2.960983, 1.789786, 1.008497, 1.004007, 0.045335, 0.032093, 0.975091, 0.726836, m_useDebugMessages);
HC_HB = new CombinedPT_calculator(  0.999654, 0.971719, 0.009076, 0.112547, 1.467668, 0.567053, 3.098246, 2.927637, 1.007833, 1.014620, 0.042205, 0.038599, 0.971439, 0.927941, m_useDebugMessages);
HM_LB = new CombinedPT_calculator(  0.992745, 0.948220, 0.028270, 0.100815, 1.680345, 0.709997, 3.140151, 1.692642, 1.000295, 1.001947, 0.010652, 0.040319, 0.258188, 0.799175, m_useDebugMessages);
HM_HB = new CombinedPT_calculator(  0.990867, 0.988128, 0.027541, 0.071765, 1.696795, 0.214829, 2.961753, 17.910572,  0.999451, 1.041677, 0.010022, 0.108073, 0.287547, 0.721288, m_useDebugMessages);
HCr_LB  = new CombinedPT_calculator(  0.995143, 0.954314, 0.064319, 0.094512, 2.047072, 0.482755, 9.502915, 2.117815, 1.001586, 1.013441, 0.026029, 0.040676, 0.515777, 0.903444, m_useDebugMessages);
HCr_HB  = new CombinedPT_calculator(  0.998967, 0.900000, 0.028295, 0.157486, 0.803407, 0.419227, 19.814205,  5.382640, 1.017054, 1.031887, 0.060562, 0.079755, 0.718215, 0.766918, m_useDebugMessages);
HF_LB = new CombinedPT_calculator(  1.000022, 0.972059, 0.009727, 0.123001, 1.255295, 0.237377, 2.840160, 3.377217, 1.006833, 1.024951, 0.045462, 0.027001, 0.906236, 0.993377, m_useDebugMessages);
HF_HB = new CombinedPT_calculator(  0.975327, 1.019038, 0.067701, 0.143776, 7.783802, 0.183061, 0.248577, 0.995200, 0.998483, 0.900035, 0.011258, 0.151595, 0.159316, 0.916354, m_useDebugMessages);


  } //  else if (m_mcPeriod == 2016) 


  return StatusCode::SUCCESS;
}



void EpCombination::getCombinedPt (double pT, double pT_Track, double energyCluster, double etaTrack, double fBrem, double &pT_Combined, double &pT_Error) const
{
  
  // our deliminations for the different transverse momentum regions
  double veryLowPtLimit_5GeV = 5.;
  double lowPtLimit_7GeV = 7.;
  double mediumPtLimit_15GeV = 15.;
  double highPtLimit_30GeV = 30.;
  
  //Rescale pT region limits if GeV not used.
  if( !m_useGeV ) {
    veryLowPtLimit_5GeV *= 1000;
    lowPtLimit_7GeV     *= 1000;
    mediumPtLimit_15GeV *= 1000;
    highPtLimit_30GeV   *= 1000;
  }
    
  pT_Error = -999.0;
  
  //Check for proper brem value.
  fBrem = fabs(fBrem);
  if (fBrem>1 )
    {
      if (m_useDebugMessages) std::cout << "WARNING: Require fBrem in [0,1]-> ptCluster and clusterResolution returned" << std::endl;
      pT_Combined=pT;
      return;
    } // if(fBrem>1 )


 if(fabs(etaTrack)>2.5)
    {
      if (m_useDebugMessages) std::cout<<"WARNING: Electron etaTrack outside of combination range, returning cluster energy and error"<< std::endl;
      pT_Combined=pT;
      return;
    }// if(fabs(etaTrack)>2.5)

  //Checks for sufficient Pt and fiducial eta.
  if(pT < veryLowPtLimit_5GeV)
    {
      if (m_useDebugMessages) std::cout<<"WARNING: Electron pT outside of combination range-> ptCluster and clusterResolution returned"<< std::endl;
      pT_Combined=pT;
      return;
    }// if(pt<lowPtLimit_7GeV)


  double  pT_Cluster = energyCluster/cosh(etaTrack); // pT from the calorimter. Still use the tracke eta measurement.

/////////////////////////////////////////////////////////////////////////////////////////

  // now the variables should be good. Funnel my pT ratios in the right histograms
  // we structure the funneling in this way: check for pt, then for eta, then for fBrem
    
    if(pT <= lowPtLimit_7GeV) // very Low pT
      {   if(fabs(etaTrack) <= 0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ vLC_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             vLC_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }             
          else if(fabs(etaTrack) <= 1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ vLM_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             vLM_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ vLCr_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             vLCr_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 2.5) // Forward eta
          {
            if(fBrem <= 0.3){ vLF_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             vLF_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
            
      }//if(pT <= mediumPtLimit_15GeV)


      else  if(pT <= mediumPtLimit_15GeV) // Low pT
      {   if(fabs(etaTrack) <= 0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ LC_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             LC_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }             
          else if(fabs(etaTrack) <= 1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ LM_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             LM_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ LCr_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             LCr_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 2.5) // Forward eta
          {
            if(fBrem <= 0.3){ LF_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             LF_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
            
      }//if(pT <= mediumPtLimit_15GeV)


      else  if(pT <= highPtLimit_30GeV) // medium pT
      {   if(fabs(etaTrack) <= 0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ MC_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             MC_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ MM_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             MM_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ MCr_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             MCr_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 2.5) // Forward eta
          {
            if(fBrem <= 0.3){ MF_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             MF_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
            
      }//if(pT <= highPtLimit_30GeV)
      else  // high pT
      {   if(fabs(etaTrack) <= 0.7) // Central Eta
          {   
            if(fBrem <= 0.2){ HC_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             HC_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 1.37) // Medium Eta
          {
            if(fBrem <= 0.3){ HM_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             HM_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 1.52) // Creck Eta
          {         
            if(fBrem <= 0.3){ HCr_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             HCr_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
          else if(fabs(etaTrack) <= 2.5) // Forward eta
          {
            if(fBrem <= 0.3){ HF_LB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // Low Brems
            else{             HF_HB->Evaluate(pT_Track,pT_Cluster,pT_Combined,pT_Error);return; } // High Brems
          }
            
      }// else // high pT


} // void EpCombination::getCombinedPt



void EpCombination::getCombinedPt (  xAOD::Electron & m_Electron ) 
{


ATH_MSG_INFO("EpCombination::getCombinedPt pt = " << m_Electron.pt() << ", pT_Track = "<< m_Electron.trackParticle()->pt() << ", E = "<< m_Electron.caloCluster()->e() << ", , eta = "<< m_Electron.eta() << ", phi = " << m_Electron.phi()); 




   // get fBremm i.e. the relative difference between the electron momentum closest to the beam, and at the last measured position in the tracker 
     
       const xAOD::TrackParticle* ptrTo_electronsTrack = m_Electron.trackParticle(); // get the TrackParticle object associated with the electron (xAOD::TrackParticle comes already with the xAOD::ElectronContainer)

       unsigned int index; // use this to index which measurement of the tracker to use
       
       ptrTo_electronsTrack->indexOfParameterAtPosition(index, xAOD::LastMeasurement); // get the pointer last track measurement point

       double trackersLastMeasP  = sqrt(std::pow(ptrTo_electronsTrack->parameterPX(index), 2) +
                                        std::pow(ptrTo_electronsTrack->parameterPY(index), 2) +
                                        std::pow(ptrTo_electronsTrack->parameterPZ(index), 2)); // calculate the momentum at the last measurement

       double trackerElectronPerigeeP = 1/(ptrTo_electronsTrack->qOverP()); //get the momentum closest to the beam, i.e. at the electron's perigee

       double fBrem = 1.0-fabs(trackersLastMeasP/trackerElectronPerigeeP); //calculate fBrem


  //save the results of the combination here
  double pT_Combined = -999.; 
  double pT_Error = -999.;

  // do the combination
  //getCombinedPt (double pT    , double pT_Track                 , double energyCluster         , double etaTrack , double fBrem, double &pT_Combined, double &pT_Error)
  getCombinedPt (m_Electron.pt(), m_Electron.trackParticle()->pt(), m_Electron.caloCluster()->e(), m_Electron.eta(),        fBrem,         pT_Combined,         pT_Error);


  m_Electron.setPt(pT_Combined); // update the pT

  ATH_MSG_INFO("EpCombination::getCombinedPt pt_combined " << m_Electron.pt() << ", pt_combined" << pT_Combined << ", pt_Error" << pT_Error ); 

}

} //namespace CP 