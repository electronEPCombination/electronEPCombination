#ifndef __COMBINEDPT_CALCULATOR_C__
#define __COMBINEDPT_CALCULATOR_C__
#include "EpCombination/CombinedPT_calculator.h"
#include <TError.h>


// Set up the likihood function that we maximize to find the combined pT

  CombinedPT_calculator::CombinedPT_calculator(Float_t meanCluster, Float_t meanTrack, Float_t sigmaCluster, Float_t sigmaTrack, Float_t aCluster, Float_t aTrack, Float_t nCluster, Float_t nTrack, 
    Float_t meanGaussCalo, Float_t meanGaussTrack, Float_t sigmaGaussCalo, Float_t sigmaGaussTrack, Float_t caloCBWeight, Float_t trackCBWeight, bool useDebug) : 
    m_useDebugMessages(useDebug)
{
    
  // Crystal Ball Parameters
  mean_cluster  = new RooRealVar("mean_cluster","mean of gaussian",meanCluster);
  mean_track    = new RooRealVar("mean_track",  "mean of gaussian",meanTrack);
  sigma_cluster = new RooRealVar("sigma_cluster","width of gaussian",sigmaCluster); 
  sigma_track   = new RooRealVar("sigma_track", "width of gaussian",sigmaTrack); 
  a_cluster     = new RooRealVar("a_cluster","a_cluster",aCluster);
  a_track       = new RooRealVar("a_track",     "a",aTrack);
  n_cluster     = new RooRealVar("n_cluster","n_cluster",nCluster);
  n_track       = new RooRealVar("n_track","n_track",nTrack);
  p_track       = new RooRealVar("p_track",   "p_track",   1.0);
  p_cluster     = new RooRealVar("p_cluster", "p_cluster", 1.0);

  // Gaussian Parameters meanGaussCalo, meanGaussTrack, sigmaGaussCalo, sigmaGaussTrack
  mean_gauss_cluster  = new RooRealVar("mean_gauss_cluster", "mean of gaussian",  meanGaussCalo);
  mean_gauss_track    = new RooRealVar("mean_gauss_track"  , "mean of gaussian",  meanGaussTrack);
  sigma_gauss_cluster  = new RooRealVar("sigma_gauss_cluster", "width of gaussian", sigmaGaussCalo); 
  sigma_gauss_track    = new RooRealVar("sigma_gauss_track"  , "width of gaussian", sigmaGaussTrack); 
  //parameters for the PDF sum
  weight_addPDF_cluster = new RooRealVar("weight_addPDF_cluster", "weight of CB in PDF sum for tracker", caloCBWeight);
  weight_addPDF_tracker = new RooRealVar("weight_addPDF_tracker", "weight of CB in PDF sum for calorimeter", trackCBWeight);

  x             = new RooRealVar("x","x", 1.0, 0., 1000000.); // independent variable, eventually what we will take as the best guess of the true pT: the combined pT
  A             = new RooFormulaVar("A","A","@0/@1",RooArgList(*p_track,  *x)); // pT_track / pT_combined
  B             = new RooFormulaVar("B","B","@0/@1",RooArgList(*p_cluster,*x)); // pT_calo  / pT_combined

  CBtrack       = new RooCBShape("CBtrack",   "crystalBall PDF", *A, *mean_track,   *sigma_track,   *a_track,   *n_track) ;  // Crystal Ball function for the tracker
  CBcluster     = new RooCBShape("CBcluster", "CBcluster",       *B, *mean_cluster, *sigma_cluster, *a_cluster, *n_cluster); // Crystal Ball function for the calorimeter

  GaussTrack       = new RooGaussian("GaussTrack",   "Gaussian PDF", *A, *mean_gauss_track,   *sigma_gauss_track  );  // Gauss function for the tracker
  GaussCluster     = new RooGaussian("GaussCluster", "Gaussian PDF", *B, *mean_gauss_cluster, *sigma_gauss_cluster);  // Gauss function for the calorimeter

  sumCBgaussTrack = new RooAddPdf("sumCBgaussTrack", "Track CB+Gauss", *CBtrack  , *GaussTrack  , *weight_addPDF_tracker); // Sum of Gauss and Crystal Ball function for the tracker
  sumCBgaussCalo  = new RooAddPdf("sumCBgaussCalo" , "Calo CB+Gauss" , *CBcluster, *GaussCluster, *weight_addPDF_cluster); // Sum of Gauss and Crystal Ball function for the calorimeter

  product       = new RooProdPdf("product", "CBtrack*CBcluster", RooArgSet(*sumCBgaussTrack, *sumCBgaussCalo));  // Product of the (Gauss Tracker + Crystall Ball Tracker) * (Gauss Calo + Crystall Ball Calo)
  prodlog       = new RooFormulaVar("prodlog", "prodlog", "-log(@0)", *product); // logarithem of the product function (we are looking for the minimum of the negative logarithm for numerical reasons)
  m             = new RooMinuit(*prodlog);  // minuit is a minimization routine


  //Silence output messages.
  if (!m_useDebugMessages) {

    m->setPrintLevel(-10000);
    m->setWarnLevel(-10000);

    RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Integration) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::LinkStateMgmt) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Caching) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Optimization) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::ObjectHandling) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Tracing) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Contents) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::DataHandling) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval);
    RooMsgService::instance().getStream(1).removeTopic(RooFit::Plotting) ;

  }//Silence output messages.


  // use these in the evaluate method below
  max = 0.;
  min = 0.;

} // CombinedPT_calculator::CombinedPT_calculator

CombinedPT_calculator::~CombinedPT_calculator()
{
  /*
  delete sigma_track;
  delete mean_track;
  delete a_track;
  delete n_track;
  delete sigma_cluster;
  delete mean_cluster;
  delete a_cluster;
  delete n_cluster;
  delete x;
  delete p_track;
  delete p_cluster;

  delete A;
  delete B;

  delete prodlog;
  delete product;

  delete m;

  delete CBtrack;
  delete CBcluster;
*/

} // CombinedPT_calculator::~CombinedPT_calculator


// here we do the actual maximizing of the likelihood for a given pT_track and pT_cluster (mathematically equivalent to minimizing the negative logarithm of the likelihood)

void CombinedPT_calculator::Evaluate(double pT_track, double pT_cluster, double &ptCombined, double &ptError) {
 
  // store the results here
  ptCombined=-999.;
  ptError=-999.;

  // copy the cluster and tracker pT values
  p_track->setVal(  pT_track);
  p_cluster->setVal(pT_cluster);


  // determine limits for the minimization process
  if(pT_track >= pT_cluster) {

    max = 1.2 * pT_track;
    min = 0.8 * pT_cluster;
    
  } else {

    max = 1.2 * pT_cluster;
    min = 0.8 * pT_track;
  }
  // if(pT_track >= pT_cluster) 

  // initial value for the minimazation process
  double _val = (pT_track+pT_cluster)/2.;

  // x is the independent variable of the likelihood function, 
  // we take the value of x for which the likelihood is maximal as the combined pt
  // so here we set the previously determined limits on x
  if (min >= x->getMax())  {
    
    x->setMax(max); // not quite sure why the sequence matters
    x->setMin(min);
    
  } else {
    
    x->setMin(min);
    x->setMax(max);
  }

  // transfert the initial for the minimazation to x
  x->setVal(_val);

  /* old section 
  //m->migrad();
  //m->hesse();
  //m->minos(RooArgSet(*x));
  */

  Int_t statusM=m->migrad(); // perform the minimazation
  if (statusM!=0)
    {
      // not sure why we do this potentially twice

      // more minimazations
      m->migrad();
      m->hesse();   
      m->minos(RooArgSet(*x));
    }
  m->hesse();
  m->minos(RooArgSet(*x));

  // product->getParameters(*x)->Print("s");
  //RooFitResult* r = m->save() ;
  
  // not sure why we have to transfer the values to intermediary variables
  Float_t xmax = x->getVal(); 
  Float_t CombinedPtError = x->getError();

  // options to get non-symmetrical errors
  //Float_t CombinedPtErrorHigh= x->getErrorHi();
  //Float_t CombinedPtErrorLow= x->getErrorLo();
 
  
  // pass by reference the results out
  ptCombined = xmax; 
  ptError = CombinedPtError;

} // void CombinedPT_calculator::Evaluate



#endif
