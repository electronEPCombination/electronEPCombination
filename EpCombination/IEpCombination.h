#ifndef EPCOMBINATION_IEPCOMBINATIONTOOL_H
#define EPCOMBINATION_IEPCOMBINATIONTOOL_H 1

#include "AsgTools/IAsgTool.h"

#include "xAODEgamma/Electron.h"   //This should be the container for electrons

//#include "xAODBase/IParticle.h"

namespace CP {

class IEpCombination : public virtual asg::IAsgTool {
   public:
      ASG_TOOL_INTERFACE( IEpCombination ) //declares the interface to athena

      // specify default values for arguments of methods in this file
      virtual void getCombinedPt(double pT, double pT_Track, double energyCluster, double etaTrack, double fBrem, double &pT_Combined, double &pT_Error) const = 0 ;

      virtual void getCombinedPt( xAOD::Electron & m_Electron ) = 0 ;

      virtual ~IEpCombination(){  }

};

} //namespace CP

#endif //> !EPCOMBINATION_IEPCOMBINATIONTOOL_H