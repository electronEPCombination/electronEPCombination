#ifndef __COMBINEDPT_CALCULATOR_H__
#define __COMBINEDPT_CALCULATOR_H__

#include "RooRealVar.h"
#include "RooFormulaVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCBShape.h"
#include "RooLandau.h"
#include "RooPlot.h"
#include "RooDataHist.h"
#include "RooUniform.h"
#include "RooProdPdf.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooMinuit.h"
#include "RooWorkspace.h"
#include "RooFitResult.h"
#include "Math/Functor.h"
//#include "Math/GSLMinimizer1D.h" // I commented this out while using a not compiled version of this class and it is still working. Though I should change this later back.
#include "RooMsgService.h"
#include <TError.h>

using namespace RooFit;

class CombinedPT_calculator  {

 public:
 

  CombinedPT_calculator(Float_t meanCluster, Float_t meanTrack, Float_t sigmaCluster, Float_t sigmaTrack, Float_t aCluster, Float_t aTrack, Float_t nCluster, Float_t nTrack, 
    Float_t meanGaussCalo, Float_t meanGaussTrack, Float_t sigmaGaussCalo, Float_t sigmaGaussTrack, Float_t caloCBWeight, Float_t trackCBWeight, bool useDebug = false); 

  ~CombinedPT_calculator();

  void Evaluate(double pT_track, double pT_cluster,double &ptCombined,double &ptError);

// let's not use this in RootCore, it makes the compile fail
//  ClassDef(CombinedPT_calculator,1) // https://root.cern.ch/how/how-use-classdef-preprocessor-macro

 private:
  
  bool m_useDebugMessages;
  double max;
  double min;
  RooRealVar* sigma_track;
  RooRealVar* mean_track;
  RooRealVar* a_track;
  RooRealVar* n_track;
  RooRealVar* sigma_cluster;
  RooRealVar* mean_cluster;
  RooRealVar* a_cluster;
  RooRealVar* n_cluster;
  RooRealVar* x;
  RooRealVar* p_track;
  RooRealVar* p_cluster;
  RooFormulaVar* A;
  RooFormulaVar* B;
  RooCBShape* CBtrack;
  RooCBShape* CBcluster;
  RooProdPdf* product;
  RooFormulaVar* prodlog;
  RooMinuit* m;


  RooRealVar* mean_gauss_cluster;
  RooRealVar* mean_gauss_track;
  RooRealVar* sigma_gauss_cluster;
  RooRealVar* sigma_gauss_track;
  RooRealVar* weight_addPDF_cluster;
  RooRealVar* weight_addPDF_tracker;

  RooGaussian* GaussTrack  ;
  RooGaussian* GaussCluster;

  RooAddPdf* sumCBgaussTrack;
  RooAddPdf* sumCBgaussCalo ;



}; // class CombinedPT_calculator

#endif