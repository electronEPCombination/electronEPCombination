#ifndef EPCOMBINATION_EPCOMBINATIONTOOL_H
#define EPCOMBINATION_EPCOMBINATIONTOOL_H 1

///
///Example usage code:
///Athena only:
///To configure the tool, add the following to your joboptions:
/// myTool = CfgMgr.EpCombination("ToolInstanceName",Property=4.0)
/// import ROOT
/// myTool.ENumProperty = ROOT.EpCombination.Val2
///To use the tool in your algorithm (can use regular ToolHandle or asg::AnaToolHandle):
/// #include "EpCombination/IEpCombination.h"
/// ToolHandle<IEpCombination> myTool("EpCombination/ToolInstanceName")
/// CHECK( myTool.retrieve() );
/// myTool->isSelected(...); //or whatever methods are implemented in IEpCombination
///
///Dual use: Alternative for c++ configuration and usage (with or without athena):
/// #include "AsgTools/AnaToolHandle.h"
/// #include "EpCombination/IEpCombination.h"
/// asg::AnaToolHandle<IEpCombination> myTool("EpCombination/ToolInstanceName");
/// myTool.setProperty( "Property", 4.0 );
/// myTool.setProperty( "ENumProperty", 
/// myTool.initialize();
/// myTool->isSelected(....); //or whatever methods are implemented in IEpCombination



#include "AsgTools/AsgTool.h"

#include "EpCombination/IEpCombination.h"
// code that does the actual CP combination
//#include "EpCombination/CombinedPT_manager.h"
#include "EpCombination/CombinedPT_calculator.h"

namespace CP {
   // DO NOT put default argument values here ... only put them in the interface class
class EpCombination: public asg::AsgTool, public virtual IEpCombination { 
 public: 
   //constructor for athena can be created using special macro
   //Note: if you add a second interface to your tool, you must use: ASG_TOOL_CLASS2( ToolName, Interface1, Interface2) 
   ASG_TOOL_CLASS( EpCombination , IEpCombination )
   //add another constructor for non-athena use cases
   EpCombination( const std::string& name );
   virtual ~EpCombination();

   /// Initialize is required by AsgTool base class
   virtual StatusCode  initialize() override;

   virtual void getCombinedPt(double pT, double pT_Track, double energyCluster, double etaTrack, double fBrem, double &pT_Combined, double &pT_Error) const override;

   virtual void getCombinedPt( xAOD::Electron & m_Electron ) override ;

 private: 

  int  m_mcPeriod;
  bool m_useDebugMessages;
  bool m_useGeV; 

  CombinedPT_calculator* vLC_LB  ;
  CombinedPT_calculator* vLC_HB  ;
  CombinedPT_calculator* vLM_LB  ;
  CombinedPT_calculator* vLM_HB  ;
  CombinedPT_calculator* vLCr_LB ;
  CombinedPT_calculator* vLCr_HB ;
  CombinedPT_calculator* vLF_LB  ;
  CombinedPT_calculator* vLF_HB  ;
  CombinedPT_calculator* LC_LB   ;
  CombinedPT_calculator* LC_HB   ;
  CombinedPT_calculator* LM_LB   ;
  CombinedPT_calculator* LM_HB   ;
  CombinedPT_calculator* LCr_LB  ;
  CombinedPT_calculator* LCr_HB  ;
  CombinedPT_calculator* LF_LB   ;
  CombinedPT_calculator* LF_HB   ;
  CombinedPT_calculator* MC_LB   ;
  CombinedPT_calculator* MC_HB   ;
  CombinedPT_calculator* MM_LB   ;
  CombinedPT_calculator* MM_HB   ;
  CombinedPT_calculator* MCr_LB  ;
  CombinedPT_calculator* MCr_HB  ;
  CombinedPT_calculator* MF_LB   ;
  CombinedPT_calculator* MF_HB   ;
  CombinedPT_calculator* HC_LB   ;
  CombinedPT_calculator* HC_HB   ;
  CombinedPT_calculator* HM_LB   ;
  CombinedPT_calculator* HM_HB   ;
  CombinedPT_calculator* HCr_LB  ;
  CombinedPT_calculator* HCr_HB  ;
  CombinedPT_calculator* HF_LB   ;
  CombinedPT_calculator* HF_HB   ;

}; 

} //namespace CP {

#endif //> !EPCOMBINATION_EPCOMBINATIONTOOL_H
