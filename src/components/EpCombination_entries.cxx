
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../electronEPCombinationTest.h"

#include "EpCombination/EpCombination.h"

DECLARE_NAMESPACE_TOOL_FACTORY( CP, EpCombination )

DECLARE_ALGORITHM_FACTORY( electronEPCombinationTest )

DECLARE_FACTORY_ENTRIES( EpCombination ) 
{
  DECLARE_ALGORITHM( electronEPCombinationTest );
  DECLARE_NAMESPACE_TOOL( CP, EpCombination );
}
