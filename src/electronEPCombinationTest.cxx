// EDM include(s):
#include "xAODEgamma/ElectronContainer.h"
#include "xAODCore/ShallowCopy.h"

// electronEPCombination includes
#include "electronEPCombinationTest.h"

//#include "xAODEventInfo/EventInfo.h"




electronEPCombinationTest::electronEPCombinationTest( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){}
/*m_electronEPCombiner( "CP::EpCombination/electronEPCombiner") {

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("electronEPCombiner",m_electronEPCombiner);
  
}
*/

electronEPCombinationTest::~electronEPCombinationTest() {}


StatusCode electronEPCombinationTest::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  //CHECK( m_electronEPCombiner.retrieve() ); //initialize the electronEPCombiner

  m_electronEPCombiner.setTypeAndName("CP::EpCombination/EpCombination");

  ANA_CHECK(m_electronEPCombiner.setProperty( "mcPeriod", 0));

  CHECK(m_electronEPCombiner.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode electronEPCombinationTest::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode electronEPCombinationTest::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  
  double ptCombined = 0 ;
  double ptError = 0 ;

  double ptCombinedReference = 0 ;


  // Test the combination algorithm. I calcylated the reference values by maximizing the underlying likelihood product in Mathematica

  // m_electronEPCombiner -> getCombinedPt(   pT,    pT_Track,    energyCluster,  etaTrack, fBrem,  &pT_Combined,  &pT_Error)
  m_electronEPCombiner -> getCombinedPt (  10000,      10000 ,            10000,        0 ,     0,    ptCombined,   ptError );
  ptCombinedReference = 10118.9;
  ATH_MSG_INFO ("Combination: ptCombined = " << ptCombined << "; Combination: ptError = " << ptError << "; Delta_reference = " << (ptCombined/ptCombinedReference -1 ) *100 << "%");

  // m_electronEPCombiner -> getCombinedPt(   pT,    pT_Track,    energyCluster,  etaTrack, fBrem,  &pT_Combined,  &pT_Error)
  m_electronEPCombiner -> getCombinedPt (   9000,      10000 ,             9000,        0 ,     0,    ptCombined,   ptError );
  ptCombinedReference = 9329.2;
  ATH_MSG_INFO ("Combination: ptCombined = " << ptCombined << "; Combination: ptError = " << ptError << "; Delta_reference = " << (ptCombined/ptCombinedReference -1 ) *100 << "%");

  // m_electronEPCombiner -> getCombinedPt(   pT,    pT_Track,    energyCluster,  etaTrack, fBrem,  &pT_Combined,  &pT_Error)
  m_electronEPCombiner -> getCombinedPt (  10000,       9000 ,            10000,        0 ,     0,    ptCombined,   ptError );
  ptCombinedReference = 10029.4;
  ATH_MSG_INFO ("Combination: ptCombined = " << ptCombined << "; Combination: ptError = " << ptError << "; Delta_reference = " << (ptCombined/ptCombinedReference -1 ) *100 << "%");

  // m_electronEPCombiner -> getCombinedPt(   pT,    pT_Track,    energyCluster,  etaTrack, fBrem,  &pT_Combined,  &pT_Error)
  m_electronEPCombiner -> getCombinedPt (   9000,       9000 ,             9000,        0 ,     0,    ptCombined,   ptError );
  ptCombinedReference = 9106.99;
  ATH_MSG_INFO ("Combination: ptCombined = " << ptCombined << "; Combination: ptError = " << ptError << "; Delta_reference = " << (ptCombined/ptCombinedReference -1 ) *100 << "%");


  // Retrieve electrons in the event
  const xAOD::ElectronContainer* electrons;
  ATH_CHECK( evtStore()->retrieve(electrons, "Electrons") );

  //Clone 
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons );
  //Record in StoreGate
  CHECK( evtStore()->record( electrons_shallowCopy.first, "ElectronCollectionCorr") );
  CHECK( evtStore()->record( electrons_shallowCopy.second, "ElectronCollectionCorrAux.") );

  //Iterate over the shallow copy
  xAOD::ElectronContainer* elsCorr = electrons_shallowCopy.first;
  xAOD::ElectronContainer::iterator el_it      = elsCorr->begin();
  xAOD::ElectronContainer::iterator el_it_last = elsCorr->end();
  unsigned int i = 0;
  for (; el_it != el_it_last; ++el_it, ++i) {
    xAOD::Electron* el = *el_it;
    ATH_MSG_INFO("Electron " << i); 
    ATH_MSG_INFO("xAOD/raw pt, eta, phi = " << el->pt() << ", " << el->eta() << ", " << el->phi() ); 

    m_electronEPCombiner -> getCombinedPt( *el ) ;

    ATH_MSG_INFO("=============END SYSTEMATICS "); 
  }

  //setFilterPassed(false); //optional: start with algorithm not passed

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //

  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram


  //setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode electronEPCombinationTest::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


