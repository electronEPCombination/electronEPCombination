AsgTool implementation of electron EP combination: electron energy measurements from the calorimeter and momentum measurements from the tracker are combined into a new value for the momentum using a maximum likelihood method.

See the included example on how to use it.

To run the included example we need to set up the tool first

```
setupATLAS

mkdir MyProject
cd MyProject

mkdir source build run

cd source/
git clone https://gitlab.cern.ch/ATLAS-EGamma/Software/Calibration/EpCombination   # you will be asked for you gitlab credentials



cd ../build
asetup AthAnalysis,21.2.5,here
mv CMakeLists.txt ../source

cmake ../source
source $TestArea/*/setup.sh


cmake --build $TestArea  # this can be called from any directory
```


You must do the following everytime you reset your environment (i.e. new shell):
```
cd MyProject/build
asetup
source $TestArea/*/setup.sh
```


To run the actual example:
```
cd run
athena electronEPCombination/electronEPCombinationTestJobOptions.py
```
